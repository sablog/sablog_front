/**
 * Created by jinc on 2016-08-14.
 */
var gulp    = require('gulp');
var uglify=require('gulp-uglify');
var useref = require('gulp-useref');
var concat = require('gulp-concat');
var cleancss=require('gulp-clean-css');
var rename=require('gulp-rename');
var clean=require('gulp-clean');

// 清除目标文件夹
gulp.task('clean', function(){
    return  gulp.src('dist/').pipe(clean());
});
// 移动html到目标文件夹
gulp.task('copyHtml',['clean'],function(){
    return gulp.src(['app/**/*.html'])
        .pipe(useref())
        .pipe(gulp.dest('dist/'));
});
//压缩js
gulp.task('minifyjs',['clean'], function() {
    gulp.src('app/**/*.js')
        .pipe(concat('all.min.js'))
        .pipe(gulp.dest('./dist/scripts'))
        .pipe(uglify());
});
//压缩css
gulp.task('minifycss',['clean'], function() {
    return gulp.src(['app/**/*.css','./themes/**/*.css'])      //压缩的文件
        .pipe(concat('all.min.css'))
        .pipe(gulp.dest('./dist/styles'))   //输出文件夹
        .pipe(cleancss());   //执行压缩
});

gulp.task('copyCss',['clean'],function(){
    return gulp.src('app/themes/**/**.*')
        .pipe(gulp.dest('./dist/themes'));
});

// 移动html到目标文件夹
gulp.task('copyTotarget',['clean','copyHtml', 'copyCss','minifyjs'],function(){
    return gulp.src(['dist/**'])  
        .pipe(gulp.dest('Y:/work/workspace/sapress/src/main/webapp/press'));
});
// 确保clean以后再执行copy
gulp.task('default', ['copyHtml', 'copyCss','minifyjs','copyTotarget'], function(){
    console.log('done');
});
