#sablog_front

前端工程创建步骤

安装node.js

省略
安装bower

npm install bower -g

执行bower

bower install 会产生bower_components

## 执行npm install命令

安装项目依赖的库。执行成功会产生node_modules  

安装gulp

先全局安装 gulp：
npm install --global gulp
再作为项目的开发依赖（devDependencies）安装： npm install --save-dev gulp

cmd中执行命令

在工程所在目录的命令行
运行npm install
然后执行gulp
 