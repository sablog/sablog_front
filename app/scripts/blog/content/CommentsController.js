/**
 * Created by jinc on 2016-07-16.
 */
angular
    .module('SABlog')
    // 最热文章controller
    .controller('CommentsController', ['$scope', '$rootScope', '$window','$state', '$stateParams', 'BlogService',
        function ($scope, $rootScope, $window,$state, $stateParams, BlogService) {
            $scope.articles=BlogService.loadLatestComments();
        }]);