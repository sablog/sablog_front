/**
 * Created by jinc on 2016-07-14.
 */
angular
    .module('SABlog')
    // 最热文章controller
    .controller('RandomArticlesController', ['$scope', '$rootScope', '$window','$state', '$stateParams', 'BlogService',
        function ($scope, $rootScope, $window,$state, $stateParams, BlogService) {
            $scope.articles=BlogService.loadRandomArticles();
            $scope.blogOnClick = function (blogId) {
                var params = {
                    id: blogId
                };
                $state.go('contentArchives', params);
            };
        }]);