/**
 * Created by sxfei on 2016/6/19.
 */
angular
    .module('SABlog')
    // 日志列表 - controller
    .controller('ContentController', ['$scope', '$rootScope', '$window','$state', '$stateParams','$sce', 'BlogService',
        function ($scope, $rootScope, $window,$state, $stateParams,$sce, BlogService) {
            $scope.viewParam={pageId:0};
            var loadContent =function(pageId){
                $scope.contentList=BlogService.loadContent(pageId);
            };


            $scope.blogOnClick = function (blogId) {
                var params = {
                    id: blogId
                };
                $state.go('contentArchives', params);
            };
            $scope.getMore=function(){
                $scope.viewParam.pageId+=1;
                loadContent($scope.viewParam.pageId);
            };

            loadContent($scope.viewParam.pageId);
        }]);