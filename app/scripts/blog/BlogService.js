/**
 * Created by sxf on 2016/2/13.
 */
angular
    .module('SABlog')
    .service('BlogService', ['BlogApi', '$sce',
        function (BlogApi, $sce) {
            var dhyyxx = {bh: false, sb: false, sbzh: true, cg: false, qrsm: false, qxsm: true, qysmyy: "1"};

            var hotArticles = [];//热门文章
            var randomArticles = [];//随机文章
            var latestComment = [];//最新评论
            var contentList = [];//文章列表
            function loadContentList(pageId) {
                BlogApi.getContentList(pageId).success(function (result) {
                    var resultList = [];
                    angular.copy(result, resultList);
                    angular.forEach(resultList, function (data) {
                        var strDes = data.contentArticle;
                        var div = document.createElement('div');
                        div.innerHTML = strDes.substr(0, 140);
                        strDes = div.innerHTML;
                        data.contentArticle = $sce.trustAsHtml(strDes);
                        //data.contentArticle = $sce.trustAsHtml(data.contentArticle);
                        contentList.push(data);
                    });
                });
                return contentList;
            }
            //加载热门文章
            function loadHotArticles() {
                BlogApi.getHotArticles().success(function (result) {
                    angular.copy(result, hotArticles);
                });
                return hotArticles;
            }

            function loadRandomArticles() {
                BlogApi.getRandomArticles().success(function (result) {
                    angular.copy(result, randomArticles);
                });
                return randomArticles;
            }

            /**
             * 加载评论
             */
            function loadLatestComments() {
                BlogApi.getLatestComments().success(function (result) {
                    angular.copy(result, latestComment);
                });
                return latestComment;
            }

            return {
                loadContent: loadContentList,
                loadRelateContentList: function (blogId) {
                    return BlogApi.getRelateContentList(blogId);
                },
                loadContentArchives: function (blogId) {
                    return BlogApi.loadContentArchives(blogId);
                },
                loadLastCommentList:function(blogId,pageNo){
                    return BlogApi.loadLastCommentList(blogId,pageNo);
                },
                loadNextContentArchives: function (blogId) {
                    return BlogApi.loadNextContentArchives(blogId);
                },
                saveComments:function(comments){
                    return BlogApi.saveComments(comments);
                },
                loadPrevContentArchives: function (blogId) {
                    return BlogApi.loadPrevContentArchives(blogId);
                },
                loadHotArticles: loadHotArticles,
                loadRandomArticles: loadRandomArticles,
                loadLatestComments: loadLatestComments
            }

        }

    ])
;