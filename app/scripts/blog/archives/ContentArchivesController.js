/**
 * Created by sxfei on 2016/6/19.
 */
angular
    .module('SABlog')
    // 日志列表 - controller
    .controller('ContentArchivesController', ['$scope', '$rootScope', '$window','$state', '$stateParams','$sce', 'BlogService',
        function ($scope, $rootScope, $window,$state, $stateParams,$sce, BlogService) {
            var contentArchives = {};
            var contentList = [];
            var comments = {};
            var commentList = [];
            $scope.comments = comments;
            $scope.vo = contentArchives;
            $scope.contentList=contentList;
            $scope.commentList =commentList;
            if($stateParams.type==='next'){
                BlogService.loadNextContentArchives($stateParams.id).success(function (data) {
                    data.contentArticle = $sce.trustAsHtml(data.contentArticle);
                    angular.copy(data, contentArchives);
                    comments.postId=contentArchives.id;
                });
            }else if($stateParams.type==='prev'){
                BlogService.loadPrevContentArchives($stateParams.id).success(function (data) {
                    data.contentArticle = $sce.trustAsHtml(data.contentArticle);
                    angular.copy(data, contentArchives);
                    comments.postId=contentArchives.id;
                });
            }else{
                BlogService.loadContentArchives($stateParams.id).success(function (data) {
                    data.contentArticle = $sce.trustAsHtml(data.contentArticle);
                    angular.copy(data, contentArchives);
                    comments.postId=contentArchives.id;
                });
            }
            //查询最新评论
            BlogService.loadLastCommentList($stateParams.id,0).success(function (data) {
                angular.copy(data, commentList);
            });
            BlogService.loadRelateContentList($stateParams.id).success(function (resultList) {
                angular.forEach(resultList, function (data) {
                    var strDes = data.contentArticle;
                    var div = document.createElement('div');
                    div.innerHTML = strDes.substr(0, 25);
                    strDes = div.innerHTML;
                    data.contentArticle = $sce.trustAsHtml(strDes);
                    //data.contentArticle = $sce.trustAsHtml(data.contentArticle);
                    contentList.push(data);
                });
                //angular.copy(data, contentList);
            });

            $scope.blogOnClick = function (blogId) {
                var params = {
                    id: blogId
                };
                $state.go('contentArchives', params);
            };
            $scope.blogPrevOnClick = function (blogId) {
                var params = {
                    id: blogId,
                    type:'prev'
                };
                $state.go('contentArchives', params);
            };
            $scope.blogNextOnClick = function (blogId) {
                var params = {
                    id: blogId,
                    type:'next'
                };
                $state.go('contentArchives', params);
            };
            $scope.saveComments = function () {
                BlogService.saveComments(comments).success(function(){
                    setTimeout(function(){
                        var params = {
                            id: comments.postId,
                            type:'next'
                        };
                        $state.go('contentArchives', params);
                    }, 1000);
                });
            };
        }]);