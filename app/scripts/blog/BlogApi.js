/**
 * Created by sxf on 2016/1/8.
 */
angular
    .module("SABlog")
    .factory("BlogApi", ["$http", "Api",
        function ($http, Api) {
            return {
                getContentList: function (pageId) {
                    return $http.get(Api.BLOG_CONTENT_LIST_URL.replace("{pageId}",pageId));
                },
                getRelateContentList: function (pageId) {
                    return $http.get(Api.BLOG_CONTENT_RELATE_LIST_URL.replace("{pageId}",pageId));
                },
                loadContentArchives: function (blogId) {
                    return $http.get(Api.BLOG_CONTENT_ARCHIVES_URL.replace("{id}",blogId));
                },
                loadLastCommentList: function (blogId,pageNo) {
                    return $http.get(Api.BLOG_ARCHIVES_COMMENTS_LIST_URL.replace("{id}",blogId).replace("{pageNo}",pageNo));
                },
                loadNextContentArchives: function (blogId) {
                    return $http.get(Api.BLOG_CONTENT_NEXT_ARCHIVES_URL.replace("{id}",blogId));
                },
                saveComments:function(comments){
                    return $http.get(Api.BLOG_COMMENT_SAVE_URL, {params: comments});
                },
                loadPrevContentArchives: function (blogId) {
                    return $http.get(Api.BLOG_CONTENT_PREV_ARCHIVES_URL.replace("{id}",blogId));
                },
                getHotArticles:function(){
                    return $http.get(Api.BLOG_HOTARTICLES_URL);
                },
                getRandomArticles:function(){
                    return $http.get(Api.BLOG_RANDOMARTICLES_URL);
                },
                getLatestComments:function(){
                    return $http.get(Api.BLOG_LATEST_COMMENTS_URL);
                }

            }
        }
    ]);