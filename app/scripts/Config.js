/**
 * Created by sxf on 2016/1/8.
 */
var APP_CFG = {
    PREFIX: "http://localhost:8081/press",
    //PREFIX: "http://sapress.duapp.com/press",
    LOCALHOST_PREFIX: location.protocol+"//"+location.hostname+ ":" +location.port+"/sablog_front"
};
/**
 * Created by sxf on 2016/1/8.
 */
angular
    .module('SABlog')
    .service('Api', function () {
        // 模式数据开关
        var mock = true;
        // 服务器真实请求url
        if (!mock) {
            return {
                    BLOG_CONTENT_LIST_URL:APP_CFG.PREFIX+'/posts/last/{pageId}',
                    BLOG_CONTENT_ARCHIVES_URL: APP_CFG.PREFIX + '/posts/{id}',
                    BLOG_CONTENT_PREV_ARCHIVES_URL: APP_CFG.PREFIX + '/posts/prev/{id}',
                    BLOG_CONTENT_NEXT_ARCHIVES_URL: APP_CFG.PREFIX + '/posts/next/{id}',
                    BLOG_HOTARTICLES_URL: APP_CFG.PREFIX+'/posts/hot',
                    BLOG_RANDOMARTICLES_URL: APP_CFG.PREFIX+'/posts/random',
                    BLOG_LATEST_COMMENTS_URL:APP_CFG.PREFIX+'/comments/allLast',
                    BLOG_CONTENT_RELATE_LIST_URL:APP_CFG.PREFIX+'/posts/relate/{pageId}',
                    BLOG_COMMENT_SAVE_URL: APP_CFG.PREFIX+'/comments/add',
                    BLOG_ARCHIVES_COMMENTS_LIST_URL: APP_CFG.PREFIX + '/comments/last/{id}/{pageNo}'

            };
        }
        // 模拟服务器请求url
        else {
            return {
                BLOG_CONTENT_LIST_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentList.json',
                BLOG_CONTENT_ARCHIVES_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentArchives.json',
                BLOG_CONTENT_PREV_ARCHIVES_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentArchives.json',
                BLOG_CONTENT_NEXT_ARCHIVES_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentArchives.json',
                BLOG_HOTARTICLES_URL: APP_CFG.LOCALHOST_PREFIX+'/app/data/blog/hotArticles.json',
                BLOG_RANDOMARTICLES_URL: APP_CFG.LOCALHOST_PREFIX+'/app/data/blog/randomArticles.json',
                BLOG_LATEST_COMMENTS_URL:APP_CFG.LOCALHOST_PREFIX+'/app/data/blog/latestComments.json',
                BLOG_CONTENT_RELATE_LIST_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentList.json',
                BLOG_COMMENT_SAVE_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/contentList.json',
                BLOG_ARCHIVES_COMMENTS_LIST_URL: APP_CFG.LOCALHOST_PREFIX + '/app/data/blog/latestComments.json',
            }
        }
    });