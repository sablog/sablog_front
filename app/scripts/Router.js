// Make sure to include the `ui.router` module as a dependency
angular.module('SABlog', [
    'ui.router'
])

    .run(
    ['$rootScope', '$state', '$stateParams',
        function ($rootScope, $state, $stateParams) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }
    ]
)

    .config(
    ['$httpProvider', '$stateProvider', '$urlRouterProvider','$locationProvider',
        function ($httpProvider, $stateProvider, $urlRouterProvider,$locationProvider) {

            /////////////////////////////
            // Redirects and Otherwise //
            /////////////////////////////

            // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
            $urlRouterProvider

                // The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
                // Here we are just setting up some convenience urls.
                .when('/c?id', '/contacts/:id')
                .when('/user/:id', '/contacts/:id')

                // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
                .otherwise('/');


            //////////////////////////
            // State Configurations //
            //////////////////////////

            // Use $stateProvider to configure your states.
            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "views/default/blog/content.html"

                })
                .state("contentArchives", {
                    url: "/contentArchives/:id",
                    params: {
                        id: "{id}",
                        type: '{type}'
                    },
                    templateUrl: "views/default/blog/content-archives.html"

                })
            ;
            //$locationProvider.html5Mode(true);
            $httpProvider.interceptors.push(['$q', '$injector', '$rootScope', function ($q, $injector, $rootScope) {
                return {
                    request: function (config) {
                        return config || $q.when(config);
                    },
                    requestError: function (request) {
                        return $q.reject(request);
                    },
                    response: function (response) {
                        if (angular.isObject(response.data) &&
                            response.data.hasOwnProperty('success') &&
                            response.data.hasOwnProperty('data')) {
                            //result is true check
                            if (response.data && response.data.success === true) {

                                response = response.data;
                            } else {
                                if (response.data.hasOwnProperty('messageCode') && response.data.hasOwnProperty('message')) {
                                    if (response.data.messageCode === 'api.secrity.unlogon') {
                                        // 调用客户端重登陆

                                    } else {
                                        app.toastr.error(response.data.messageCode + ":" + response.data.message);
                                    }
                                }
                                //app.$state.go("error");
                                return $q.reject(response);
                            }
                        }
                        return response || $q.when(response);
                    },
                    responseError: function (response) {
                        return $q.reject(response);
                    }
                };

            }]);
        }
    ]
);
